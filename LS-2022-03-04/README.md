# Commit hases used for Ampere Mt. Jade LinuxBoot:


https://github.com/AmpereComputing/edk2-ampere-tools.git

`e67a6acc165c3d9c38037d6ea4acded5b03b2951`

---

https://github.com/AmpereComputing/edk2.git

`0a13d9704cd9013ed499af7e85cf50526bd0dca1`

---

https://github.com/AmpereComputing/edk2-platforms.git

`3176fd35a159033010fbd28feb15bec22e843f69`

---

https://github.com/linuxboot/linuxboot.git

`c96c441b8fc83685fb31f43931164f88c7183f37`

---

https://github.com/u-root/u-root.git

`9f92f5fdfac0fe23765281cd87b92ed7238de54e`

---
